package uk.co.rajaconsulting.pedal

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
@RunWith(classOf[JUnitRunner])
class HTTPRequestSuite extends FunSuite with ShouldMatchers {

  test("Complete headers should parse correctly") {
    val completeHeaders = "Header1: First Header\r\nHeader2: second\nHeader3:third\n\n"
    val expectedCompleteHeaders = collection.mutable.LinkedHashMap("Header1" -> "First Header", "Header2" -> "second", "Header3" -> "third")
    val req = new HTTPRequest
    req.append(completeHeaders.getBytes)
    req.processedHeaders should be(expectedCompleteHeaders)
  }

  test("Request should be complete") {
    val requestString = "Content-Length: 12\r\nHeader: head\r\n\r\n123456789101112"
    val req = new HTTPRequest
    req.append(requestString.getBytes)
    req.requestComplete should be(true)
  }

  test("Request should throw 411") {
    val excep = evaluating {
      val requestString = "Header: head\r\n\r\n123456789101112"
      val req = new HTTPRequest
      req.append(requestString.getBytes)
      req.requestComplete
    } should produce [BadRequestException]
    excep.code should be (411)
  }
}

/**
Copyright 2011 Sumit Raja. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY SUMIT RAJA ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SUMIT RAJA OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Sumit Raja.
*/