package uk.co.rajaconsulting.pedal
import java.nio.charset.Charset
import grizzled.slf4j.Logging
import org.joda.time.DateTime
import uk.co.rajaconsulting.pedal.Constants._
import java.nio.channels.WritableByteChannel
import java.nio.ByteBuffer

class HTTPRequest extends Logging {
  private val headerSplit = """(\r\n|\n)""".r
  private val headerValueSplit = """\s*[\:|=]\s*""".r
  private var buffer: List[Byte] = Nil
  private var headerIndex: Int = -1
  private var headers: Map[String, String] = new collection.immutable.ListMap()
  private var verb: Symbol = null

  def headersRecved = headerIndex != -1

  def contentEncoding = headers.get(CONTENT_ENCODING) match {
    case None => null
    case Some(enc) => enc
  }

  def contentLength = headers.get(CONTENT_LENGTH) match {
    case None => -1
    case Some(cl) => Integer.parseInt(cl)
  }

  def requestComplete = {
    if (headerIndex == -1) {
      false
    } else {
      val contentLength = headers.get(CONTENT_LENGTH) match {
        case None => throw BadRequestException(411, "Length required")
        case Some(n) => Integer.parseInt(n)
      }
      buffer.slice(headerIndex + 2, buffer.size).size >= contentLength
    }
  }
  def body = if (headerIndex == -1) None else Some(buffer.slice(headerIndex + 2, buffer.size))

  def processedHeaders = headers

  def append(data: Array[Byte]) = {
    buffer ++= data
    if (headerIndex == -1) {
      headerIndex = processBuffer(HEADER_END, HEADER_END_COMPAT, data.size, { arr =>
        extractHeaders(arr)
      })
    }
  }

  def processBuffer(terminator: String, terminatorAlt: String, dataSize: Int, handler: (Array[Byte]) => Unit) = {
    val scanStartIndex = if (buffer.size - dataSize > 2) buffer.size - dataSize - 2 else buffer.size - dataSize // extra two to catch a trailing line end from before
    var index = buffer.indexOfSlice(terminator, scanStartIndex)
    if (index == -1) {
      index = buffer.indexOfSlice(terminatorAlt, scanStartIndex)
    }
    if (index > -1) {
      handler(buffer.slice(0, index).toArray)
    }
    debug("buffer.size=" + buffer.size + " data.size=" + dataSize + " scanStartIndex=" + scanStartIndex + " index=" + index)
    index
  }

  private def extractHeaders(buf: Array[Byte]) = {
    val headerString = new String(buf, "ISO-8859-1")
    val splits = headerSplit.split(headerString)
    headers ++= splits.map { line =>
      val a = headerValueSplit.split(line)
      if (a.size <= 1 || a.size > 2) {
        throw BadRequestException(400, "Invalid header " + line)
      }
      (a(0) -> a(1))
    }
  }

  case class Conditions(val modifiedSince: DateTime, val unmodifiedSince: DateTime, val ifMatch: String, val noneMatch: String, val range: Range)
}

class HTTPResponse(val code: Int, val statusLine: String) extends Logging {
  private var headers = Map("Connection" -> "close", "Server" -> SERVER)
  private val buffer = ByteBuffer.allocate(1024)
  def write(channel: WritableByteChannel) = {
    buffer.put((HTTP_VERSION + " ").getBytes)
    buffer.asIntBuffer.put(code)
    buffer.put((" " + statusLine + HEADER_END).getBytes)
    buffer.rewind
    channel.write(buffer)
  }
}

case class BadRequestException(code: Int, message: String) extends Exception(message)

/**
Copyright 2011 Sumit Raja. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY SUMIT RAJA ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SUMIT RAJA OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Sumit Raja.
*/