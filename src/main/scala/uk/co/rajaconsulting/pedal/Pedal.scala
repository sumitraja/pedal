package uk.co.rajaconsulting.pedal
import java.nio.channels._
import java.nio._
import scala.util.continuations._
import java.net._
import scala.collection.JavaConversions._
import java.nio.channels.SelectionKey._
import scala.actors.Actor._
import java.nio.charset.Charset
import scala.collection.mutable.SynchronizedQueue
import java.io.IOException
import grizzled.slf4j.Logging
import uk.co.rajaconsulting.pedal.Constants._

/**
 * @author Sumit Raja
 */
object Pedal extends Logging {
  type QueueFunction = { def apply(): Unit }
  type Callback = (SelectionKey) => Unit

  val selector = Selector.open
  var processed = false;

  val messageQueue = new SynchronizedQueue[QueueFunction]

  def main(args: Array[String]) {
    println(SERVER)
    val channel = ServerSocketChannel.open()

    channel.configureBlocking(false)
    channel.socket.bind(new InetSocketAddress(2233))
    reset {
      val sock = accept(channel)
      val request = read(sock)
      respond(sock, request)
      close(sock)
    }

    while (true) {
      doSelect
    }
    println("Bye")
  }

  case class ReadMessage(val channel: SocketChannel, val callback: () => Unit)

  def accept(channel: ServerSocketChannel): SocketChannel @suspendable = {
    shift { k =>
      registerCallback(channel, OP_ACCEPT, { key =>
        if (key.isAcceptable) {
          val sock = channel.accept
          sock.configureBlocking(false)
          k(sock)
        }
      })

    }
  }

  def read(channel: SocketChannel): HTTPRequest @suspendable = {

    shift { k: ((HTTPRequest) => Unit) =>
      val request = new HTTPRequest
      val buffer = ByteBuffer.allocate(1024)
      registerCallback(channel, OP_READ, { key =>
        if (key.isReadable) {
          buffer.clear
          try {
            val n = channel.read(buffer)
            buffer.flip
            if (n == -1) {
              request.headersRecved match {
                case true => k(request)
                case false => new CloseSocketWithErrorMessage(channel, new BadRequestException(400, "Bad Request")) +: messageQueue // deal with socket closure immediately
              }
            } else if (n > 0) {
              val array = new Array[Byte](n)
              buffer.get(array)
              request.append(array)
            }
          } catch {
            case e: BadRequestException =>
            case e: IOException => new CloseSocketMessage(channel) +: messageQueue
          }
        }
      })
    }
  }

  def respond(channel: SocketChannel, request: HTTPRequest): Unit @suspendable = {
    shift { k: (Unit => Unit) =>
      registerCallback(channel, OP_WRITE, { key =>
        if (key.isWritable) {
          channel.write(ByteBuffer.wrap("HTTP/1.1 200 All Good\nContent-Length: 0\nConnection: Close\n\n".getBytes))
          k()
        }
      })
    }
  }

  def close(channel: SocketChannel): Unit = {
    val str = channel.toString
    channel.keyFor(selector).cancel();
    channel.close
    info("200")
  }

  def registerCallback(channel: SelectableChannel, key: Int, callback: Callback) = {
    if (channel.isOpen) {
      val v = channel.register(selector, key, callback)
    }
  }

  def doSelect {
    selector.select
    val keys = selector.selectedKeys.toList
    selector.selectedKeys.clear
    keys.foreach { key =>
      if (key.isValid) {
        key.attachment.asInstanceOf[Callback](key)
      }
    }
    messageQueue.foreach { fun =>
      fun()
    }
  }

  class CloseSocketMessage(channel: SocketChannel) {
    def apply() = {
      close(channel)
    }
  }

  class CloseSocketWithErrorMessage(channel: SocketChannel, ex: BadRequestException) extends CloseSocketMessage(channel) {
    override def apply() = {
      info(ex.code + " " + ex.message)
      channel.write(ByteBuffer.wrap((ex.code + " " + ex.message + "\r\n\r\n").getBytes))
      super.apply()
    }
  }
}


/**
Copyright 2011 Sumit Raja. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY SUMIT RAJA ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SUMIT RAJA OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Sumit Raja.
*/
