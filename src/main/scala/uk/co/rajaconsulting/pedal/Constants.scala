package uk.co.rajaconsulting.pedal

object Constants {

  val SERVER = "Pedal 0.1"
  val HTTP_VERSION = "HTTP/1.0"

  val CRLF = "\r\n"
  val LF = "\n"
  val HEADER_END = CRLF + CRLF
  val HEADER_END_COMPAT = LF + LF
  val CONTENT_ENCODING = "Content-Encoding"
  val CONTENT_LENGTH = "Content-Length"
  val METHODS = Map('GET -> "GET", 'HEAD -> "HEAD", 'POST -> "POST", 'PUT -> "PUT", 'DELETE -> "DELETE")

}